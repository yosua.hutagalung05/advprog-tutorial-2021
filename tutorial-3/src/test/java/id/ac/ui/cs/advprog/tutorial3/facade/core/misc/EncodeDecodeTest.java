package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EncodeDecodeTest {

    @Test
    public void testEncodeDecodeHasEncodeMethod() throws Exception {
        Class<?> encodeDecodeClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.misc.EncodeDecode");
        Method encode = encodeDecodeClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testEncodeDecodeHasDecodeMethod() throws Exception {
        Class<?> encodeDecodeClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.misc.EncodeDecode");
        Method decode = encodeDecodeClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

}
