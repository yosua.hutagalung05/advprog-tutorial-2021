package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransformationTest {
    private Class<?> transformationClass;

    @BeforeEach
    public void setUp() throws Exception {
        transformationClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation");
    }

    @Test
    public void testTransformationIsAPublicInterface() {
        int classModifiers = transformationClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testTransformationHasEncodeAbstractMethod() throws Exception {
        Class<?>[] encodeArgs = new Class[1];
        encodeArgs[0] = Spell.class;
        Method encode = transformationClass.getDeclaredMethod("encode", encodeArgs);
        int methodModifiers = encode.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, encode.getParameterCount());
    }

    @Test
    public void testTransformationHasDecodeAbstractMethod() throws Exception {
        Class<?>[] decodeArgs = new Class[1];
        decodeArgs[0] = Spell.class;
        Method decode = transformationClass.getDeclaredMethod("decode", decodeArgs);
        int methodModifiers = decode.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, decode.getParameterCount());
    }
}
