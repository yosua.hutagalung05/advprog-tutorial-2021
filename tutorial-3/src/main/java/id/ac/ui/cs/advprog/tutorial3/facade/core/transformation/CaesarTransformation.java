package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation implements Transformation {
    private int key;

    public CaesarTransformation(int key) {
        this.key = key;
    }

    public CaesarTransformation() {
        this.key = 4;
    }

    @Override
    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    @Override
    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        StringBuilder result = new StringBuilder();
        String text = spell.getText();
        int shift = encode ? this.key : 26 - this.key;

        for (int i = 0; i < text.length(); i++) {
            if (Character.isLetter(text.charAt(i)) && Character.isUpperCase(text.charAt(i))) {
                char ch = (char)(((int)text.charAt(i) + shift - 65) % 26 + 65);
                result.append(ch);
            } else if (Character.isLetter(text.charAt(i)) && !Character.isUpperCase(text.charAt(i))){
                char ch = (char)(((int)text.charAt(i) + shift - 97) % 26 + 97);
                result.append(ch);
            } else {
                result.append(text.charAt(i));
            }
        }
        return new Spell(result.toString(), spell.getCodex());
    }
}
