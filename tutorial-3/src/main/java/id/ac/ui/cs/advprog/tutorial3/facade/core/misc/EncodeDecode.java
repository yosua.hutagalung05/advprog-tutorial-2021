package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;

import java.util.ArrayList;
import java.util.List;

public class EncodeDecode {
    private List<Transformation> transformations;

    public EncodeDecode() {
        this.transformations = new ArrayList<>();
        transformations.add(new CaesarTransformation());
        transformations.add(new AbyssalTransformation());
        transformations.add(new CelestialTransformation());
    }

    public Spell encode(String text) {
        Spell result = new Spell(text, AlphaCodex.getInstance());

        for (Transformation transformation : this.transformations) {
            result = transformation.encode(result);
        }

        return CodexTranslator.translate(result, RunicCodex.getInstance());
    }

    public Spell decode(String text) {
        Spell result = CodexTranslator.translate(new Spell(text, RunicCodex.getInstance()), AlphaCodex.getInstance());

        for (int i = this.transformations.size()-1; i >= 0; i--) {
            result = transformations.get(i).decode(result);
        }

        return result;
    }
}
