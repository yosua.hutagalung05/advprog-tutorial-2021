package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.*;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)
    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private LogRepository logRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        for (Bow bow : this.bowRepository.findAll()) {
            this.weaponRepository.save(new BowAdapter(bow));
        }

        for (Spellbook spellbook : this.spellbookRepository.findAll()) {
            this.weaponRepository.save(new SpellbookAdapter(spellbook));
        }

        return this.weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon currentWeapon = this.weaponRepository.findByAlias(weaponName);

        if (attackType == 0) {
            String holderName = currentWeapon.getHolderName();
            String attackDescription = currentWeapon.normalAttack();
            this.logRepository.addLog(holderName + " attacked with " + weaponName +
                    " (normal attack): " + attackDescription);
        } else {
            String holderName = currentWeapon.getHolderName();
            String attackDescription = currentWeapon.chargedAttack();
            this.logRepository.addLog(holderName + " attacked with " + weaponName +
                    " (charged attack): " + attackDescription);
        }

        this.weaponRepository.save(currentWeapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return this.logRepository.findAll();
    }
}
