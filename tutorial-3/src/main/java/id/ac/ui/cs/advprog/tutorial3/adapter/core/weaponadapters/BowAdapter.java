package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean isAimShot;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.isAimShot = false;
    }

    @Override
    public String normalAttack() {
        return this.bow.shootArrow(this.isAimShot);
    }

    @Override
    public String chargedAttack() {
        if (!this.isAimShot) {
            this.isAimShot = true;
            return "Entered Aim Shot mode";
        } else {
            this.isAimShot = false;
            return "Leaving Aim Shot mode";
        }
    }

    @Override
    public String getName() {
        return this.bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return this.bow.getHolderName();
    }
}
