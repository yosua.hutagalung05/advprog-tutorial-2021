package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isChargedTwiceInARow;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.isChargedTwiceInARow = false;
    }

    @Override
    public String normalAttack() {
        this.isChargedTwiceInARow = false;
        return this.spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (this.isChargedTwiceInARow) {
            return "Can't use large spell twice in a row!!!";
        } else {
            this.isChargedTwiceInARow = true;
            return this.spellbook.largeSpell();
        }
    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }

}
