package id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;

public class MondoUdonFactory implements MenuFactory {
    @Override
    public Noodle addNoodle() {
        return new Udon();
    }

    @Override
    public Meat addMeat() {
        return new Chicken();
    }

    @Override
    public Topping addTopping() {
        return new Cheese();
    }

    @Override
    public Flavor addFlavor() {
        return new Salty();
    }


}
