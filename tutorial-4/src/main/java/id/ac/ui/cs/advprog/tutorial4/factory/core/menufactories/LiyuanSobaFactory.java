package id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;

public class LiyuanSobaFactory implements MenuFactory {
    @Override
    public Noodle addNoodle() {
        return new Soba();
    }

    @Override
    public Meat addMeat() {
        return new Beef();
    }

    @Override
    public Topping addTopping() {
        return new Mushroom();
    }

    @Override
    public Flavor addFlavor() {
        return new Sweet();
    }


}
