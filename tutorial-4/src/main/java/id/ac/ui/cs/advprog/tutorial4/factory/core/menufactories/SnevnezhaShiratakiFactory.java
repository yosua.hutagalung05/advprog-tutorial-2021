package id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;

public class SnevnezhaShiratakiFactory implements MenuFactory {
    @Override
    public Noodle addNoodle() {
        return new Shirataki();
    }

    @Override
    public Meat addMeat() {
        return new Fish();
    }

    @Override
    public Topping addTopping() {
        return new Flower();
    }

    @Override
    public Flavor addFlavor() {
        return new Umami();
    }


}
