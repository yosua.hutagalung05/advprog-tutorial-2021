package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;
    private InuzumaRamen inuzumaRamen;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
        inuzumaRamen = new InuzumaRamen("InuzumaRamenRes");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenGetNameReturnCorrectly(){
        String name = inuzumaRamen.getName();
        assertEquals(name, "InuzumaRamenRes");
    }

    @Test
    public void testInuzumaRamenGetNoodleReturnCorrectly(){
        Noodle noodle = inuzumaRamen.getNoodle();
        assertNotNull(noodle);
    }

    @Test
    public void testInuzumaRamenGetMeatReturnCorrectly(){
        Meat meat = inuzumaRamen.getMeat();
        assertNotNull(meat);
    }

    @Test
    public void testInuzumaRamenGetToppingReturnCorrectly(){
        Topping topping = inuzumaRamen.getTopping();
        assertNotNull(topping);
    }

    @Test
    public void testInuzumaRamenGetFlavorReturnCorrectly(){
        Flavor flavor = inuzumaRamen.getFlavor();
        assertNotNull(flavor);
    }

}