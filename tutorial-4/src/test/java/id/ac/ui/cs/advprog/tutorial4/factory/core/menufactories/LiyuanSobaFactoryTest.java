package id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaFactoryTest {
    private Class<?> liyuanSobaFactoryClass;
    private LiyuanSobaFactory liyuanSobaFactory;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories.LiyuanSobaFactory");
        liyuanSobaFactory = new LiyuanSobaFactory();
    }

    //Menguji Class
    @Test
    public void testLiyuanSobaFactoryIsAConcreteClass() {
        assertFalse(Modifier.
                isAbstract(liyuanSobaFactoryClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories.MenuFactory")));
    }

    //Menguji Method
    @Test
    public void testLiyuanSobaFactoryOverrideAddNoodle() throws Exception {
        Method getDescription = liyuanSobaFactoryClass.getDeclaredMethod("addNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideAddMeat() throws Exception {
        Method getDescription = liyuanSobaFactoryClass.getDeclaredMethod("addMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideAddTopping() throws Exception {
        Method getDescription = liyuanSobaFactoryClass.getDeclaredMethod("addTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryOverrideAddFlavor() throws Exception {
        Method getDescription = liyuanSobaFactoryClass.getDeclaredMethod("addFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryAddNoodleMethodReturnCorrectly() {
        Noodle noodle = liyuanSobaFactory.addNoodle();
        String description = noodle.getDescription();
        assertEquals(description, "Adding Liyuan Soba Noodles...");
    }

    @Test
    public void testLiyuanSobaFactoryAddMeatMethodReturnCorrectly() {
        Meat meat = liyuanSobaFactory.addMeat();
        String description = meat.getDescription();
        assertEquals(description, "Adding Maro Beef Meat...");
    }

    @Test
    public void testLiyuanSobaFactoryAddToppingMethodReturnCorrectly() {
        Topping topping = liyuanSobaFactory.addTopping();
        String description = topping.getDescription();
        assertEquals(description, "Adding Shiitake Mushroom Topping...");
    }

    @Test
    public void testLiyuanSobaFactoryAddFlavorMethodReturnCorrectly() {
        Flavor flavor = liyuanSobaFactory.addFlavor();
        String description = flavor.getDescription();
        assertEquals(description, "Adding a dash of Sweet Soy Sauce...");
    }
}
