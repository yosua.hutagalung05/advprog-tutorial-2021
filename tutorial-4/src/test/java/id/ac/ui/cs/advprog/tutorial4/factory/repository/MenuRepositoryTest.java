package id.ac.ui.cs.advprog.tutorial4.factory.repository;


import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Repository
public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @Mock
    private List<Menu> menus;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
        menus = new ArrayList<>();
        Menu inuzumaRamen = new InuzumaRamen("Inuzuma Ramen");
        menus.add(inuzumaRamen);
    }

    @Test
    public void whenMenuRepoGetMenusItShouldReturnMenuList() {
        ReflectionTestUtils.setField(menuRepository, "list", menus);
        List<Menu> acquiredMenus = menuRepository.getMenus();

        assertThat(acquiredMenus).isEqualTo(menus);
    }

    @Test
    public void whenMenuRepoAddMenuItShouldSaveMenu() {
        ReflectionTestUtils.setField(menuRepository, "list", menus);
        Menu mondoUdon = new MondoUdon("Mondo Udon");
        menuRepository.add(mondoUdon);
        List<Menu> acquiredMenus = menuRepository.getMenus();

        assertThat(acquiredMenus).isEqualTo(menus);
        assertThat(menus.get(1)).isEqualTo(mondoUdon);
    }
}

