package id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonFactoryTest {
    private Class<?> mondoUdonFactoryClass;
    private MondoUdonFactory mondoUdonFactory;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories.MondoUdonFactory");
        mondoUdonFactory = new MondoUdonFactory();
    }

    //Menguji Class
    @Test
    public void testMondoUdonFactoryIsAConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonFactoryClass.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories.MenuFactory")));
    }

    //Menguji Method
    @Test
    public void testMondoUdonFactoryOverrideAddNoodle() throws Exception {
        Method getDescription = mondoUdonFactoryClass.getDeclaredMethod("addNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideAddMeat() throws Exception {
        Method getDescription = mondoUdonFactoryClass.getDeclaredMethod("addMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideAddTopping() throws Exception {
        Method getDescription = mondoUdonFactoryClass.getDeclaredMethod("addTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverrideAddFlavor() throws Exception {
        Method getDescription = mondoUdonFactoryClass.getDeclaredMethod("addFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryAddNoodleMethodReturnCorrectly() {
        Noodle noodle = mondoUdonFactory.addNoodle();
        String description = noodle.getDescription();
        assertEquals(description, "Adding Mondo Udon Noodles...");
    }

    @Test
    public void testMondoUdonFactoryAddMeatMethodReturnCorrectly() {
        Meat meat = mondoUdonFactory.addMeat();
        String description = meat.getDescription();
        assertEquals(description, "Adding Wintervale Chicken Meat...");
    }

    @Test
    public void testMondoUdonFactoryAddToppingMethodReturnCorrectly() {
        Topping topping = mondoUdonFactory.addTopping();
        String description = topping.getDescription();
        assertEquals(description, "Adding Shredded Cheese Topping...");
    }

    @Test
    public void testMondoUdonFactoryAddFlavorMethodReturnCorrectly() {
        Flavor flavor = mondoUdonFactory.addFlavor();
        String description = flavor.getDescription();
        assertEquals(description, "Adding a pinch of salt...");
    }
}
