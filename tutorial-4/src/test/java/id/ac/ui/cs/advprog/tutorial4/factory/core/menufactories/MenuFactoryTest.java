package id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuFactoryTest {
    private Class<?> menuFactoryInterface;

    @BeforeEach
    public void setup() throws Exception {
        menuFactoryInterface = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories.MenuFactory");
    }

    @Test
    public void testMenuFactoryIsAPublicInterface() {
        int classModifiers = menuFactoryInterface.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMenuFactoryHasAddFlavorAbstractMethod() throws Exception {
        Method getDescription = menuFactoryInterface.getDeclaredMethod("addFlavor");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMenuFactoryHasAddMeatAbstractMethod() throws Exception {
        Method getDescription = menuFactoryInterface.getDeclaredMethod("addMeat");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMenuFactoryHasAddToppingAbstractMethod() throws Exception {
        Method getDescription = menuFactoryInterface.getDeclaredMethod("addTopping");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testMenuFactoryHasAddNoodleAbstractMethod() throws Exception {
        Method getDescription = menuFactoryInterface.getDeclaredMethod("addNoodle");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }
}