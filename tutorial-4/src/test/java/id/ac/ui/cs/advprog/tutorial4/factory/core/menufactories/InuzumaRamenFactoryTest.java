package id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenFactoryTest {
    private Class<?> inuzumaRamenFactoryClass;
    private InuzumaRamenFactory inuzumaRamenFactory;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories.InuzumaRamenFactory");
        inuzumaRamenFactory = new InuzumaRamenFactory();
    }

    //Menguji Class
    @Test
    public void testInuzumaRamenFactoryIsAConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenFactoryClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories.MenuFactory")));
    }

    //Menguji Method
    @Test
    public void testInuzumaRamenFactoryOverrideAddNoodle() throws Exception {
        Method getDescription = inuzumaRamenFactoryClass.getDeclaredMethod("addNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideAddMeat() throws Exception {
        Method getDescription = inuzumaRamenFactoryClass.getDeclaredMethod("addMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideAddTopping() throws Exception {
        Method getDescription = inuzumaRamenFactoryClass.getDeclaredMethod("addTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryOverrideAddFlavor() throws Exception {
        Method getDescription = inuzumaRamenFactoryClass.getDeclaredMethod("addFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryAddNoodleMethodReturnCorrectly() {
        Noodle noodle = inuzumaRamenFactory.addNoodle();
        String description = noodle.getDescription();
        assertEquals(description, "Adding Inuzuma Ramen Noodles...");
    }

    @Test
    public void testInuzumaRamenFactoryAddMeatMethodReturnCorrectly() {
        Meat meat = inuzumaRamenFactory.addMeat();
        String description = meat.getDescription();
        assertEquals(description, "Adding Tian Xu Pork Meat...");
    }

    @Test
    public void testInuzumaRamenFactoryAddToppingMethodReturnCorrectly() {
        Topping topping = inuzumaRamenFactory.addTopping();
        String description = topping.getDescription();
        assertEquals(description, "Adding Guahuan Boiled Egg Topping");
    }

    @Test
    public void testInuzumaRamenFactoryAddFlavorMethodReturnCorrectly() {
        Flavor flavor = inuzumaRamenFactory.addFlavor();
        String description = flavor.getDescription();
        assertEquals(description, "Adding Liyuan Chili Powder...");
    }
}