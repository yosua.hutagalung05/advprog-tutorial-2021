package id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiFactoryTest {
    private Class<?> snevnezhaShiratakiFactoryClass;
    private SnevnezhaShiratakiFactory snevnezhaShiratakiFactory;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories.SnevnezhaShiratakiFactory");
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }

    //Menguji Class
    @Test
    public void testSnevnezhaShiratakiFactoryIsAConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiFactoryClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menufactories.MenuFactory")));
    }

    //Menguji Method
    @Test
    public void testSnevnezhaShiratakiFactoryOverrideAddNoodle() throws Exception {
        Method getDescription = snevnezhaShiratakiFactoryClass.getDeclaredMethod("addNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideAddMeat() throws Exception {
        Method getDescription = snevnezhaShiratakiFactoryClass.getDeclaredMethod("addMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideAddTopping() throws Exception {
        Method getDescription = snevnezhaShiratakiFactoryClass.getDeclaredMethod("addTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverrideAddFlavor() throws Exception {
        Method getDescription = snevnezhaShiratakiFactoryClass.getDeclaredMethod("addFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryAddNoodleMethodReturnCorrectly() {
        Noodle noodle = snevnezhaShiratakiFactory.addNoodle();
        String description = noodle.getDescription();
        assertEquals(description, "Adding Snevnezha Shirataki Noodles...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryAddMeatMethodReturnCorrectly() {
        Meat meat = snevnezhaShiratakiFactory.addMeat();
        String description = meat.getDescription();
        assertEquals(description, "Adding Zhangyun Salmon Fish Meat...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryAddToppingMethodReturnCorrectly() {
        Topping topping = snevnezhaShiratakiFactory.addTopping();
        String description = topping.getDescription();
        assertEquals(description, "Adding Xinqin Flower Topping...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryAddFlavorMethodReturnCorrectly() {
        Flavor flavor = snevnezhaShiratakiFactory.addFlavor();
        String description = flavor.getDescription();
        assertEquals(description, "Adding WanPlus Specialty MSG flavoring...");
    }
}
