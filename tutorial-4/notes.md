**Perbedaan lazy instantiation dengan eager instantiation**  
  
Sebuah class singleton yang menggunakan lazy instantiation akan menginstatiate dirinya sendiri pada saat pemanggilan
pertama class tersebut atau pada saat class tersebut pertama kali akan digunakan. Berbeda dengan eager instantiaton
di mana sebuah class singleton akan langsung menginstantiasi dirinya sendiri pada saat program atau sistem pertama
kali dijalankan.  
  
Kelebihan dari lazy instantiation adalah penggunaan resource yang lebih hemat dalam sebuah aplikasi skala besar
karena semua class singleton hanya akan di instantiate pada saat diperlukan saja. Kekurangannya adalah munculnya
isu multithreading di mana sebuah class singleton bisa terinstantiate lebih dari sekali karena penggunaan
thread yang berbeda. Selain itu, penggunaan synchronized membuat program akan memakan banyak resource jika
traffic dari class singleton sedang tinggi.  
  
Kelebihan dari eager instantiation adalah class singleton akan selalu siap untuk digunakan ketika dibutuhkan.
Selain itu, implementasi dari eager instantiation sedikit lebih mudah dari lazy instantiation walaupun keduanya
termasuk sangat mudah untuk diimplentasi. Kekurangannya adalah, karena di instantiate pada saat program atau
sistem pertama kali dijalankan, otomatis akan memakan banyak resource jika programnya berskala besar. Resource
tersebut akan terbuang sia-sia jika pada akhirnya class singleton tersebut tidak terpakai dalam menjalankan
program.