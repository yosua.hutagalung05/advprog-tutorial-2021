package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    @Override
    public String defend() {
        return "Armorku tebal!";
    }

    @Override
    public String getType() {
        return "Defend with armor";
    }
}
